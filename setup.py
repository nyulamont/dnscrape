#!/usr/bin/env python
from setuptools import setup, find_packages

with open('README.md') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='dnscrape',
    version='0.0.1',
    description='Scrape DNS info',
    long_description=readme,
    author='Lamont Nelson',
    author_email='lamont.nelson@nyu.edu',
    url='',
    license=license,
    packages=find_packages(exclude=('tests', 'docs'))
)
