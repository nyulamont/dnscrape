import logging
import random
from multiprocessing.pool import ThreadPool

import dns.resolver
import msgpack

import redis_pool
from work_items import Item, ItemType

class ResolverHost(dns.resolver.Resolver):
    def __init__(self, config):
        super().__init__(configure=False)
        self.dns_providers = config['dns_servers']
        self.rotate = True
        for provider in config['dns_servers']:
            ips = self.dns_providers[provider]
            logging.info("adding nameserver: {l}".format(l=ips))
            self.nameservers.extend(ips)

class DnsProcessor:
    RECORD_TYPES = ['A', 'AAAA', 'MX', 'CNAME', 'NS', 'TXT', 'SOA']
    ITEM_KEY = 'dns'

    def __init__(self, config, work_mgr):
        self.resolver = ResolverHost(config)
        self.work_mgr = work_mgr
        self.pool = ThreadPool(processes=len(DnsProcessor.RECORD_TYPES))

    def next_domain(self):
        item = self.work_mgr.next_item(ItemType.domain)
        if item is not None:
            return item.name
        else:
            return None

    def add_result(self, results, rdatatype, val):
        rdatatype = rdatatype.encode('utf-8')
        if rdatatype not in results:
            results[rdatatype] = []
        logging.debug("adding %s -> %s", rdatatype, val)
        results[rdatatype].append(val)

    def do_query(self, domain, rdtype='A'):
        logging.debug("do query %s for %s", rdtype, domain)
        ADDITIONAL_RDCLASS = 65535
        request = dns.message.make_query(domain, rdtype)
        request.flags |= dns.flags.AD
        request.find_rrset(request.additional, dns.name.root,
                           ADDITIONAL_RDCLASS, dns.rdatatype.OPT, 
                           create=True, force_unique=True)
        
        response = None
        done = False
        ntry = 0
        while not done:
            ns = random.choice(self.resolver.nameservers)
            logging.info("query ns %s; try %d", ns, ntry)
            timeout = 1.1 ** ntry

            try:
                response = dns.query.udp(request, ns, timeout=timeout)
            except dns.exception.Timeout as e:
                logging.debug("timeout exception: %s", e)
            except Exception as e:
                logging.debug("unknown exception: %s", e)
            finally:
                ntry = ntry + 1
            if ntry == 10 or response is not None:
                done = True

        
        if response is None:
            return None 

        logging.debug("answer %s", response.answer)
        logging.debug("additional %s", response.additional)
        logging.debug("authority %s", response.authority)

        answers = []
        answers.extend(response.answer)
        answers.extend(response.additional)
        answers.extend(response.authority)

        results = {'domain': domain}
        for a in answers:
            logging.debug("answer %s", a.to_text())
            for i in a.items:
                if a.rdtype == dns.rdatatype.A:
                    rdtype = "A"
                    self.add_result(results, rdtype, i.address)
                elif a.rdtype == dns.rdatatype.AAAA:
                    rdtype = "AAAA"
                    self.add_result(results, rdtype, i.address)
                elif a.rdtype == dns.rdatatype.MX:
                    rdtype = "MX"
                    self.add_result(results, rdtype, [i.preference,
                                                      str(i.exchange)])
                elif a.rdtype == dns.rdatatype.CNAME:
                    rdtype = "CNAME"
                    self.add_result(results, rdtype, str(i.target))
                elif a.rdtype == dns.rdatatype.NS:
                    rdtype = "NS"
                    self.add_result(results, rdtype, str(i.target))
                elif a.rdtype == dns.rdatatype.TXT:
                    rdtype = "TXT"
                    self.add_result(results, rdtype, i.strings)
                elif a.rdtype == dns.rdatatype.SOA:
                    rdtype = "SOA"
                    self.add_result(results, rdtype, [str(i.mname),
                                                      str(i.rname),
                                                      i.serial, i.refresh,
                                                      i.retry, i.expire,
                                                      i.minimum])
                else:
                    logging.info("unknown type %d", a.rdtype)

        return results 

    def persist(self, redis, results):
        domain = results['domain']
        logging.debug("persist domain %s: %s", domain, results)
        item = Item.load(domain, ItemType.domain, redis)
        if self.ITEM_KEY in item.properties:
            item.properties[self.ITEM_KEY] = { **item.properties[self.ITEM_KEY], **results }
        else:
            item.properties[self.ITEM_KEY] = results

        item.save(redis)
        
    def tick(self, config):
        conn = redis_pool.get_connection()
        domain = self.next_domain()
        logging.info("dns: next domain %s", domain)
        if domain is not None:
            futures = []
            for rdtype in self.RECORD_TYPES:
                # results = self.do_query(domain, rdtype)
                future = self.pool.apply_async(self.do_query, (domain, rdtype))
                futures.append(future)

            for future in futures:
                results = future.get()
                if results is not None and len(results.keys()) > 1:
                    self.persist(conn, results)
