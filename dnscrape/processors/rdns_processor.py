import logging
import random
import traceback
import ipaddress
import time
from multiprocessing.pool import ThreadPool

import dns.resolver
import dns.reversename
import msgpack

import redis_pool
from work_items import Item, ItemType, WorkItemsMgr
from processors import dns_processor

class RDnsProcessor:
    TODO_KEY = "work:todo:{t}"
    PROCESSED_DOMAINS_KEY = "rdns:domains:processed" 
    UNPROCESSED_DOMAINS_KEY = "rdns:domains:todo"
    BLOCK_SIZE = 1

    def __init__(self, config, work_mgr):
        self.resolver = dns_processor.ResolverHost(config) 
        self.config = config
        self.work_mgr = work_mgr
        self.dns_processor = dns_processor.DnsProcessor(config, work_mgr)
        self.redis = redis_pool.get_connection() 
        self.domains_key = self.work_mgr.PROCESSING_KEY.format(t=ItemType.domain)
        self.todo_key = self.TODO_KEY.format(t=ItemType.ip)
        self.processed_domains_key = self.PROCESSED_DOMAINS_KEY
        pass

    def next_ip(self):
        item = self.work_mgr.next_item(ItemType.ip)
        return item
    
    def check_processed(self, domain):
        return self.redis.sismember(self.processed_domains_key, domain.name)
    
    def add_items(self, domain):
        logging.info("rdns add items %s", domain.name)
        a_utf8 = 'A'.encode('utf-8')
        if 'dns' not in domain.properties:
            logging.info("dns info not found")
            return True 

        dns_properties = domain.properties['dns']
        if a_utf8 not in dns_properties:
            return False
        else:
            logging.info(domain.properties['dns'][a_utf8])
        
        A = dns_properties[a_utf8]
        logging.debug(A)

        for ip in A: 
            try:
                ip = ipaddress.ip_address(ip)
                item = Item.load(ip.exploded, ItemType.ip, self.redis)
                if item is None:
                    item = Item(ip.exploded, ItemType.ip, {
                        'source': [],
                        'rdns': {} 
                    })
                    source = item.properties['source']
                    source.append(domain.name) 
                    self.work_mgr.queue_item(item)
                else:
                    logging.debug("existing ip %s", item.name)
                
                if 'source' not in item.properties:
                    item.properties['source'] = []

                source = item.properties['source']
                if domain.name not in source:
                    source.append(domain.name)
                    item.save(self.redis)
                    logging.debug("modified ip adress %s from %s", item.name, domain.name)
                
            except ValueError:
                logging.error("not an ip address: %s", ip)
            except:
                traceback.print_exc()
        return True


    def load_new_ips(self):
        domains = []
        if self.redis.scard(self.UNPROCESSED_DOMAINS_KEY) == 0:
            logging.info("loading new domains for rdns")
            self.redis.sdiffstore(self.UNPROCESSED_DOMAINS_KEY,
                                  Item.set_key(ItemType.domain),
                                  self.PROCESSED_DOMAINS_KEY)
        
        domains = self.redis.srandmember(self.UNPROCESSED_DOMAINS_KEY,
                                         self.BLOCK_SIZE)
        logging.info("domains: %s", domains)
        for d in domains:
            unpacked_d = msgpack.unpackb(d, encoding='utf-8')
            #d = d.decode('utf-8')
            domain = Item.load(unpacked_d, ItemType.domain, self.redis)
            if domain is not None and not self.check_processed(domain):
                if self.add_items(domain):
                    logging.info("remove %s from rdns unprocessed", d)
                    self.redis.srem(self.UNPROCESSED_DOMAINS_KEY, d)

    def do_rdns_query(self, ip):
        time.sleep(0.1)
        rname = dns.reversename.from_address(ip.name)
        request = dns.message.make_query(rname, "PTR")
        ns = random.choice(self.resolver.nameservers)
        try:
            logging.info("rdns query %s", rname)
            response = dns.query.udp(request, ns, timeout=3)
            logging.info(response.answer)
            results = []
            for ptr in response.answer:
                name = str(ptr[0])
                domain = Item.load(name, ItemType.domain, self.redis)
                if domain is None:
                    logging.info("new domain %s", name)
                    domain = Item(name, ItemType.domain, 
                                  { 'source': [ ip.name ], 'rdns': {} })
                    self.work_mgr.queue_item(domain)
                    results.append(domain.name)
            return results 
        except dns.exception.Timeout as e:
            logging.debug("timeout exception: %s", e)
        except Exception as e:
            logging.debug("unknown exception: %s", e)
        finally:
            pass

    def tick(self, config):
        logging.debug(self.__class__.__name__ + ".tick")
        self.load_new_ips()
        
        ip = self.next_ip()
        logging.info("next ip: %s", ip)
        if ip is not None:
            results = self.do_rdns_query(ip) 
            logging.info("rdns results for %s: %s", ip.name, results)
        else:
            logging.debug("ip is NONE!!!!!!!!!")
