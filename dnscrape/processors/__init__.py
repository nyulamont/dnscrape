import importlib
from . import dns_processor
from . import rdns_processor

_processors = []
def register_processor(obj):
    _processors.append(obj)

def processors():
    global _processors
    return _processors

def init(config, work_mgr):
    register_processor(dns_processor.DnsProcessor(config, work_mgr))
    register_processor(rdns_processor.RDnsProcessor(config, work_mgr))
