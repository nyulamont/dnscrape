import sys
import yaml
from collections import MutableMapping

class Config(MutableMapping):
    _instance = None 
    
    def instance():
        if Config._instance is None:
            Config._instance = Config()
        return Config._instance

    def __init__(self, fn=None):
        self.__data = None
        if fn is not None:
            self.load(fn)
    
    def load(self, yaml_path):
        try:
            print("loading config...")
            with open(yaml_path) as f:
                self.__data = yaml.load(f)
                return True
        except IOError:
            print("error loading config file '{fn}'".format(fn=yaml_path), file=sys.stderr)
            return False

    def __iter__(self):
        return self.__data.__iter__()

    def __getitem__(self, key):
        return self.__data.__getitem__(key)

    def __setitem__(self, key, value):
        return self.__data.__setitem__(key, value)

    def __delitem__(self, key):
        return self.__data.__delitem__(key)

    def __len__(self):
        return self.__data.__len__()
