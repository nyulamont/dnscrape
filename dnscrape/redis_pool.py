from config import Config
import redis

def init():
    global pool 
    _config = Config.instance()
    print(_config.__dict__)
    host = _config['redis']['host']
    port = _config['redis']['port']
    pool = redis.ConnectionPool(host=host, port=port, db=0)

def get_connection():
    return redis.Redis(connection_pool=pool)

def release(conn):
    pool.release(conn)
