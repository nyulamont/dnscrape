import igraph as ig
import numpy as np

# based on the description of the MCL algorithm from the slides at
# https://www.cs.ucsb.edu/~xyan/classes/CS595D-2009winter/MCL_Presentation2.pdf
# 
# See http://micans.org/mcl/ for a full description

MAX_ITERATIONS = 10

def converge(M):
    print("converge")
    diff = (M ** 2) - M
    d = np.max(diff) - np.min(diff)
    diff = None
    if d==0:
        return True
    return False

def expand(M, e=2):
    print("expand")
    return M ** e

def inflate(M, r=2):
    print("inflate")
    return normalize(np.power(M, r))

def self_loop(M):
    np.fill_diagonal(M, 2)
    return M

def normalize(M):
    return M / np.sum(M, axis=0)

def mcl(M, e=2, r=2):
    print("create matrix")
    M = np.matrix(M)
    print("self loop")
    M = self_loop(M)
    print("start normalize")
    M = normalize(M)
    i=0
    done = False
    while i < MAX_ITERATIONS and not done:
        i += 1
        M = expand(M, e)
        M = inflate(M, r)
        if converge(M):
            done = True
        else:
            print("iteration {}".format(i))
    print("{} iterations".format(i))
    return M
