#!/usr/bin/env python
import sys
if sys.version_info < (3, 0):
    raise("must use python 3.0 or greater")

import logging
import dns.resolver
import redis_pool
from config import Config
from scraper import Scraper
import work_items
from work_items import WorkItemsMgr
import processors

LOG_LEVEL=logging.INFO

if __name__ == '__main__':
    logging.basicConfig(level=LOG_LEVEL)
    if len(sys.argv) != 2:
        print("usage: {p} <yaml-config>".format(p=sys.argv[0]))
        sys.exit(1)

    config_file = sys.argv[1]
    logging.info("loading configuration from {f}".format(f=config_file))
    config = Config.instance()
    config.load(config_file)
    redis_pool.init()
    redis = redis_pool.get_connection()
    wim = WorkItemsMgr(config, redis)
    processors.init(config, wim)
    redis = redis_pool.get_connection()
    s = Scraper(config, wim, redis)
    s.run()
