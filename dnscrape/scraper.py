import logging
import random
import time
import redis
import processors
import work_items
from config import Config

SLEEP_TICK = 0.0125 

class Scraper:
    DOMAINS_KEY = 'scraper.domains'

    def __init__(self, config, work_mgr, redis):
        logging.debug(config['redis'])
        self.sleep_time = float(config['scrape']['sleep']) 
        self.servers = config['dns_servers']
        self.domain_file = config['domain_file']
        self.work_mgr = work_mgr
        self.redis = redis
        logging.debug("dns servers:\n%s", self.servers)

    def run(self):
        self.import_domains()
        plist = processors.processors()
        while True:
            for p in plist:
                p.tick(Config.instance())
                t = self.sleep_time + random.random() * self.sleep_time
                logging.debug("sleep for %2.4f", t)
                time.sleep(t)

    def import_domains(self):
        logging.info("import domains from %s", self.domain_file)
        redis = self.redis
        with open(self.domain_file) as f:
            for domain in f:
                domain = domain.strip()
                work_item = work_items.Item(domain,
                                            work_items.ItemType.domain)
                if not work_item.exists(redis):
                    logging.info("new domain: %s", work_item.name)
                    self.work_mgr.queue_item(work_item)

