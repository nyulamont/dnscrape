import mcl
import igraph as ig


def square(n):
    m = [ [0]*n for _ in range(n) ]
    return m

with open('full_graph_final.gml','r') as f:
    g=ig.Graph.Read(f, 'gml')
    m = square(len(g.vs))
    c=0

    print("{} edges".format(len(g.es)))
    for e in g.es:
        m[e.source][e.target] = 1
        c += 1
        if c%10000 == 0:
            print("edge # {}".format(c))
    g = None
    #M = g.get_adjacency().data
    #print ("after getadj")
    #g=None
    M=mcl.mcl(m)
    print(M)

