import logging
import hashlib

import msgpack
import redis_pool

class ItemType:
    domain = 1
    ip = 2

NUM_BUCKETS = 4096 

def bucket_num(v):
    hsh = hashlib.sha256()
    hsh.update(v)
    bucket = int(hsh.hexdigest(), 16) % NUM_BUCKETS
    return bucket

class Item:
    BUCKET_KEY = "work_item:{bucket}"
    SET_KEY = "work_item:all:{item_type}"
    ITEM_KEY = "{item_type}_{name}"

    def load(name, item_type, redis):
        result = Item(name, item_type)
        if result.exists(redis):
            encoded_item = redis.hget(result.bucket_key(), result.key())
            if encoded_item is not None:
                d = msgpack.unpackb(encoded_item, encoding='utf-8')
                result.__dict__ = d
                logging.debug("loaded item %s", result.__dict__)
                return result
        else:
            logging.debug("%s %d does not exist", name, item_type)
            return None

    def __init__(self, name, item_type, properties=None):
        if properties is None:
            properties = {}
        if type(name) is str:
            self.name = name
        else:
            self.name = name.decode('utf-8')
        self.item_type = item_type
        self.properties = properties

    def key(self):
        return self.ITEM_KEY.format(name=self.name, item_type=self.item_type) 

    def set_key(item_type):
        return Item.SET_KEY.format(item_type=item_type)
    
    def bucket_key(self):
        if type(self.name) is str:
            bucket  = bucket_num(self.name.encode('utf-8'))
        else:
            bucket = bucket_num(self.name)
        return self.BUCKET_KEY.format(bucket=bucket)

    def save(self, redis):
        redis.hset(self.bucket_key(),
                   self.key(),
                   msgpack.packb(self.__dict__, use_bin_type=True))
        redis.sadd(Item.set_key(self.item_type), msgpack.packb(self.name,
                                                               use_bin_type=True))
        logging.info("saving %s", self.__dict__)

    def exists(self, redis):
        logging.debug("check exists: %s, %s",
                      self.bucket_key(),
                      self.key())
        result = redis.hexists(self.bucket_key(),
                               self.key())
        logging.debug("result %s", result)
        return result


class WorkItemsMgr:
    TODO_KEY="work:todo:{t}"
    PROCESSING_KEY="work:processing:{t}"

    def __init__(self, config, redis):
        self.config = config
        self.redis = redis

#        xxx = self.redis.lrange("work:processing:1", 0, 10**6)
#        for x in xxx:
#            self.redis.sadd("work_item:all:1", x)

    
    def next_item(self, item_type):
        logging.debug("next item %s", item_type)
        todo_list_key = self.TODO_KEY.format(t=item_type)
        processing_list_key = self.PROCESSING_KEY.format(t=item_type)
        v = self.redis.brpoplpush(todo_list_key, processing_list_key, 1)
        logging.debug(v)
        if v is not None:
            name = msgpack.unpackb(v, use_list=False, encoding='utf-8')
            logging.debug("name: %s", name)
            item = Item.load(name, item_type, self.redis)
            if item is not None:
                logging.info("return next %s", item.__dict__)
            return item
        else:
            return None
    

    def queue_item(self, item):
        if not item.exists(self.redis):
            item.save(self.redis)
        todo_key = self.TODO_KEY.format(t=item.item_type)
        self.redis.lpush(todo_key,
                         msgpack.packb(item.name, use_bin_type=True))
        logging.info("queue item %s %s", todo_key, item.name)
        return [todo_key, item.name]
