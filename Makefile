VENV := .venv

$(VENV):
	virtualenv $(VENV)

init: $(VENV) 
	$(VENV)/bin/pip install -r requirements.txt
