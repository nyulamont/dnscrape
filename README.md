This is a collection of scripts to collect dns info from a set of nameservers.

## Collected Data
The data collected with this program is available [here](http://lamontnelson.com/nyu/security-privacy/dns.tgz). It includes the final redis database (.aof file) and the graph extracted from the data in .gml format. The redis database more metadata than is included in the graph to conserve memory. The graph includes 2.7M vertices and 7.5M edges. 

Unfortunately, the MCL implementation included here is not able to finish analyzing this graph, even with 32GB of RAM. The algorithm is functionally correct, but inefficient. Next steps include exporting the graph data from the igraph library into a sparse array implementation, implement a pruning strategy for MCL, and possibly using a faster implementation language than python for analysis of the data.

## Install Requirements
* redis: http://redis.io
* Python 3.5.1 (with pip and virtualenv installed as packages)

## Install Instructions
### Steps:

* install redis according to instructions at http://redis.io. Alternatively, run the redis-server without installing.
* make sure virtualenv is installed 
* create a virtualenv in the root directory of the project 
* install required python packages 

### Commands:
Commands should be executed from the root of the project.

	pip install virtualenv
	virtualenv .venv
	.venv/bin/pip install -r requirements.txt

## Execution
### Start the redis server:
	redis-server conf/redis.conf &
### Run one or more processes of the scraper:
	bin/dnscrape conf/dnscrape.yml