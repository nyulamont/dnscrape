#!/usr/bin/env bash
[ ! -f dnscrape/__init__.py ] && echo 'run from root of project!' && exit 1
[ ! -d .venv ] && make init
.venv/bin/python $@
